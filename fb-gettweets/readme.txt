=== FB User Tweets ===
Contributors: zdevil09
Tags: Twitter, Feed, API
Requires at least: 4.0
Tested up to: 4.2.2
Stable tag: trunk
License: GPLv2

FB Get Tweets plugin uses the Twitter API to get tweets from a user\'s timeline and display them on your WordPress site.

== Description ==
FB Get Tweets plugin uses the Twitter API to get tweets from a user\'s timeline and display them on your WordPress site. You need to create a Twitter application and generate a user access token.
FB Get Tweets Plugin provides a nice interface to add the settings, view cached tweets and style the output.
The plugin saves the tweets using WordPress transient. You can set the refresh time (expiration) from the settings interface.

== Installation ==
Upload the fb-gettweets folder to the /wp-content/plugins/ directory
Activate the FB Get Tweets plugin through the \'Plugins\' menu in WordPress
Create a Twitter app https://apps.twitter.com/
Generate Access Token
Configure the plugin (Twitter API KEY and User Token) by going to the FB Tweets Settings
Configure the plugin stye by going to the FB Tweets Style
Configure the plugin stye by going to the FB Tweets Style
Call function fb_get_tweets() for raw data (objects) or format_tweet for html

== Screenshots ==
1. Settings Page
2. Styling Page
3. Cached Tweets